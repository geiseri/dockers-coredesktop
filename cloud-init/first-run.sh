#!/bin/bash

echo "First run...."

function create_user {
  if [ "$ADMIN_USER"x != "x" ]; then
    echo ' --admin-user \"$ADMIN_USER\" --admin-pass \"$ADMIN_PASSWORD\" '
  fi
}

function configure_database {
  if [ "$MYSQL_SERVER_ENV_MYSQL_USER"x != "x" ]; then
    echo ' --database mysql --database-name \"$MYSQL_SERVER_ENV_MYSQL_DATABASE\" --database-host \"$MYSQL_SERVER_PORT_3306_TCP_ADDR\" --database-user \"$MYSQL_SERVER_ENV_MYSQL_USER\" --database-pass \"$MYSQL_SERVER_ENV_MYSQL_PASSWORD\" '
  else
    echo " --database sqlite "
  fi
}

function configure_reverse_proxy {

  if [ "$OVERWRITE_HOST"x != "x" ]; then

  fi

  if [ "$OVERWRITE_PROTOCOL"x != "x" ]; then

  fi

  if [ "$OVERWRITE_WEBROOT"x != "x" ]; then

  fi

  if [ "$OVERWRITE_CONDADDR"x != "x" ]; then

  fi

}

configure_reverse_proxy

su www-data -s /bin/bash -c 'php /var/www/owncloud/occ maintenance:install $(configure_database) $(create_user) -n' || exit 1

exit 0
