# Base graphical docker container

# Configuration

To configure the desktop you need to create a cloud-init
[No Cloud](http://cloudinit.readthedocs.org/en/latest/topics/datasources.html#no-cloud) data store.

## meta-data:

```
instance-id: iid-local01
local-hostname: cloudimg
```

## user-data:

```
#cloud-config
# vim: syntax=yaml

groups: 
  - developers
timezone: $TIMEZONE
users: 
  - gecos: "Docker Session User"
    groups: 
      - developers
      - users
    lock-passwd: false
    name: $USERNAME
    passwd: $PASSWORD_HASH

# required for NLA support in RDP
runcmd:
  - "openssl req -x509 -newkey rsa:2048 -nodes -keyout /etc/xrdp/key.pem -out /etc/xrdp/cert.pem -days 365 -subj '/CN=dockerdesktop'"
```

This docker uses systemd internally so the ``--cap-add SYS_ADMIN`` and ``-v /sys/fs/cgroup:/sys/fs/cgroup:ro`` are
required to let that work.  

```
docker run -dti --name=development --cap-add SYS_ADMIN \
           -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
           -v /path/to/cloud-init:/var/lib/cloud/seed/nocloud \
           -p 3389:3389 geiseri/coredesktop
```
